import java.util.List;

public class Bishop extends AbstractPiece {
    public Bishop(Player player, AbstractBoardSquare square) {
        super(player, square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        return null;
    }
}
