public abstract class AbstractBoardSquare {
    private final int x;
    private final int y;

    public AbstractBoardSquare(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
