import java.util.List;

public class Pawn extends AbstractPiece {

    public Pawn(Player player, AbstractBoardSquare square) {
        super(player, square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        return null;
    }
}
