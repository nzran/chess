import java.util.List;

public class Queen extends AbstractPiece{
    public Queen(Player player, AbstractBoardSquare square) {
        super(player, square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        return null;
    }
}
