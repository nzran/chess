import java.util.ArrayList;
import java.util.List;

public class Board {
    // Changed BoardSquare to AbstractBoardSquare and resolved
    // Dependency Inversion

    /*
     * Since Board and AbstractPiece both need to know the same
     * BoardSquare, we can remove this dependency by moving
     * the BoardSquare held by the piece to AbstractPiece, reducing coupling.
     *
     * Now we can simply hold list of pieces instead of
     * a Map of BoardSquare and AbstractPiece.
     */

    private List<AbstractPiece> pieces = new ArrayList<>();

    /*
     * This can be omitted as it is redundant
     * when we have BoardSquare on AbstractPiece instead
    */
    //private BitSet occupiedSquares;

    private List<King> kings = new ArrayList<>();

    /*
     * Board will be the Creator of BoardSquares and Pieces.
    */
    private List<List<AbstractBoardSquare>> boardSquares = new ArrayList<>();

    public Board() {
        // Create 8x8 BoardSquares as 2D List
        for (int i = 0; i < 8; i++) {
            // Create 8 BoardSquares
            List<AbstractBoardSquare> row = new ArrayList<>();
            for (int j = 0; j < 8; j++) {
                // Create BoardSquare
                row.add(new BoardSquare(i, j));
            }
            boardSquares.add(row);
        }

        // Create Players
        Player player1 = new Player("white");
        Player player2 = new Player("black");

        // Create 32 Pieces
        // Create Pawns
        for (int i = 0; i < 8; i++) {
            pieces.add(new Pawn(player1, boardSquares.get(1).get(i)));
            pieces.add(new Pawn(player2, boardSquares.get(6).get(i)));
        }
        // Create Rooks
        pieces.add(new Rook(player1, boardSquares.get(0).get(0)));
        pieces.add(new Rook(player1, boardSquares.get(0).get(7)));
        pieces.add(new Rook(player2, boardSquares.get(7).get(0)));
        pieces.add(new Rook(player2, boardSquares.get(7).get(7)));
        // Create Knights
        pieces.add(new Knight(player1, boardSquares.get(0).get(1)));
        pieces.add(new Knight(player1, boardSquares.get(0).get(6)));
        pieces.add(new Knight(player2, boardSquares.get(7).get(1)));
        pieces.add(new Knight(player2, boardSquares.get(7).get(6)));
        // Create Bishops
        pieces.add(new Bishop(player1, boardSquares.get(0).get(2)));
        pieces.add(new Bishop(player1, boardSquares.get(0).get(5)));
        pieces.add(new Bishop(player2, boardSquares.get(7).get(2)));
        pieces.add(new Bishop(player2, boardSquares.get(7).get(5)));
        // Create Queens
        pieces.add(new Queen(player1, boardSquares.get(0).get(3)));
        pieces.add(new Queen(player2, boardSquares.get(7).get(3)));
        // Create Kings
        King whiteKing = new King(player1, boardSquares.get(0).get(4));
        King blackKing = new King(player2, boardSquares.get(7).get(4));
        kings.add(whiteKing);
        kings.add(blackKing);
        pieces.add(whiteKing);
        pieces.add(blackKing);
    }

    public boolean movePiece(AbstractPiece p, AbstractBoardSquare b) {
        return false;
    }

    // We only need one method for this as every piece is AbstractPiece
    public List<AbstractBoardSquare> getValidMoves(AbstractPiece p) {
        return null;
    }

    public boolean isEmpty(AbstractBoardSquare s) {
        return false;
    }

}
