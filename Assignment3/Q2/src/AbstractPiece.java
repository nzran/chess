import java.util.List;

public abstract class AbstractPiece {
    /*
     * Added getters and setters for startSquare and currentSquare
     * Added constructor to set team, this also lets us make Team final
     * and avoid changing teams mid-game as that should be impossible for
     * chess piece.
     *
     * Switched from BoardSquare to AbstractBoardSquare
     * Switched Team to Player, as that is what it is called in the class diagram
     */

    final Player player;
    private final AbstractBoardSquare startSquare;
    private AbstractBoardSquare currentSquare;

    public AbstractPiece(Player player, AbstractBoardSquare startSquare) {
        this.player = player;
        this.startSquare = startSquare;
        this.currentSquare = startSquare;
    }

    public abstract List<AbstractBoardSquare> getEmptyBoardMoves();

    public AbstractBoardSquare getStartSquare() {
        return startSquare;
    }

    public void setCurrentSquare(AbstractBoardSquare square) {
        this.currentSquare = square;
    }

    public AbstractBoardSquare getCurrentSquare() {
        return this.currentSquare;
    }

   }
