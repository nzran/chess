import java.util.List;

public class King extends AbstractPiece {
    /*
     * Added constructor to set team
     * Switched from BoardSquare to AbstractBoardSquare
     */

    public King(Player player, AbstractBoardSquare square) {
        super(player, square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        return null;
    }
}
