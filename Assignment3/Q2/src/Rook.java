import java.util.List;

public class Rook extends AbstractPiece {
    public Rook(Player player, AbstractBoardSquare square) {
        super(player, square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        return null;
    }
}
