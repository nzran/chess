public class Player {
    private final String name;

    public Player(String name) {
        this.name = name;
    }

    public void takeNextTurn(){

    }

    public String getName() {
        return name;
    }
}
