import java.util.BitSet;
import java.util.List;
import java.util.Map;

public class Board {
    private Map<BoardSquare, AbstractPiece> pieces;
    private BitSet occupiedSquares;
    private Map<King, AbstractPiece> kings;

    public boolean movePiece(AbstractPiece p, BoardSquare b) {
        return false;
    }

    public List<BoardSquare> getValidMoves(King k) {
        return null;
    }

    public List<BoardSquare> getValidMoves(Pawn p) {
        return null;
    }

    public boolean isEmpty(BoardSquare s) {
        return false;
    }

}
