import java.util.List;

public abstract class AbstractPiece {
    Team team;
    private BoardSquare startSquare;
    private BoardSquare currentSquare;

    public abstract List<BoardSquare> getEmptyBoardMoves();
}
