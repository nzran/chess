package test;

import main.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    /*
     * For this test, we have three scenarios for each piece:
     * 1. Piece is at starting square (0,0)
     * 2. Piece is somewhat at middle of board with maximum possible moves (4,4)
     * 3. Piece is at the edge of board and somewhat in the middle (4,0)
     */

    Board board;
    List<AbstractPiece> pieces = new ArrayList<>();
    Player player;

    @BeforeEach
    void setUp() {
        board = new Board();
        pieces = new ArrayList<>();
        player = new Player("player", board);
    }

    @Test
    void knightAtFourFourTakeNextTurn() {
        // Knight (4,4) can go in any direction but will take the first
        // move at bottom left of board (3, 2)
        AbstractPiece knight = new Knight(board.boardSquares().get(4).get(4));
        player.addPiece(knight);
        player.takeNextTurn();
        assertArrayEquals(new int[]{3,2}, knight.getCurrentSquare().getCoordinates());
    }

    @Test
    void knightAtZeroZeroTakeNextTurn() {
        // Knight (0,0) will take the available move on right to (2 ,1)
        AbstractPiece knight = new Knight(board.boardSquares().get(0).get(0));
        player.addPiece(knight);
        player.takeNextTurn();
        assertArrayEquals(new int[]{2, 1}, knight.getCurrentSquare().getCoordinates());
    }

    @Test
    void knightAtFourZeroTakeNextTurn() {
        // Knight (4,0) will take the available move on
        // left and one row up to (2 ,1)
        AbstractPiece knight = new Knight(board.boardSquares().get(0).get(4));
        player.addPiece(knight);
        player.addPiece(knight);
        player.takeNextTurn();
        assertArrayEquals(new int[]{2, 1}, knight.getCurrentSquare().getCoordinates());
    }

    @Test
    void bishopAtFourFourTakeNextTurn() {
        // Bishop (4,4) can go in any direction but will take the first
        // move at bottom left of board (3, 3)
        AbstractPiece bishop = new Bishop(board.boardSquares().get(4).get(4));
        player.addPiece(bishop);
        player.takeNextTurn();
        assertArrayEquals(new int[]{0,0}, bishop.getCurrentSquare().getCoordinates());
    }

    @Test
    void bishopAtZeroZeroTakeNextTurn() {
        // Bishop (0,0) will take the first available move on (1 ,1)
        AbstractPiece bishop = new Bishop(board.boardSquares().get(0).get(0));
        player.addPiece(bishop);
        player.takeNextTurn();
        assertArrayEquals(new int[]{1, 1}, bishop.getCurrentSquare().getCoordinates());
    }

    @Test
    void bishopAtFourZeroTakeNextTurn() {
        // Bishop (4,0) will take the available move on
        // left and one row up to (3 ,1)
        AbstractPiece bishop = new Bishop(board.boardSquares().get(0).get(4));
        System.out.println(Arrays.toString(bishop.getCurrentSquare().getCoordinates()));
        player.addPiece(bishop);
        player.takeNextTurn();
        System.out.println(Arrays.toString(bishop.getCurrentSquare().getCoordinates()));
        assertArrayEquals(new int[]{3, 1}, bishop.getCurrentSquare().getCoordinates());
    }
}