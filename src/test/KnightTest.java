package test;

import main.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class KnightTest {

    @BeforeEach
    void setUp() {

    }

    // Test has 100% line coverage

    @Test
    void knightAtZeroZero() {
        // Test for moves if knight is at corner
        // Knight at (0, 0) and should be able to move to (1, 2) and (2, 1)
        AbstractPiece knight = new Knight(new BoardSquare(0,0));
        List<AbstractBoardSquare> emptyBoardMoves = knight.getEmptyBoardMoves();
        int[][] emptyCoordinates = new int[emptyBoardMoves.size()][2];
        for (int i = 0; i < emptyBoardMoves.size(); i++) {
            emptyCoordinates[i] = emptyBoardMoves.get(i).getCoordinates();
        }
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{1,2})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{2,1})));
    }

    @Test
    void knightAtThreeTwo() {
        // Tests for all 8 possible moves a knight can have a time
        // Knight at (3, 2) and should be able to move to (1, 1), (1, 3), (2, 0), (2, 4), (4, 0), (4, 4), (5, 1), (5, 3)
        AbstractPiece knight = new Knight(new BoardSquare(3,2));
        List<AbstractBoardSquare> emptyBoardMoves = knight.getEmptyBoardMoves();
        int[][] emptyCoordinates = new int[emptyBoardMoves.size()][2];
        for (int i = 0; i < emptyBoardMoves.size(); i++) {
            emptyCoordinates[i] = emptyBoardMoves.get(i).getCoordinates();
        }
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{1,1})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{1,3})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{2,0})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{2,4})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{4,0})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{4,4})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{5,1})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{5,3})));
    }

    @Test
    void knightAtSevenSeven() {
        // Test for moves if knight is at other side corner
        // Knight at (7, 7) and should be able to move to (5, 6) and (6, 5)
        AbstractPiece knight = new Knight(new BoardSquare(7,7));
        List<AbstractBoardSquare> emptyBoardMoves = knight.getEmptyBoardMoves();
        int[][] emptyCoordinates = new int[emptyBoardMoves.size()][2];
        for (int i = 0; i < emptyBoardMoves.size(); i++) {
            emptyCoordinates[i] = emptyBoardMoves.get(i).getCoordinates();
        }
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{5,6})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{6,5})));
    }

    @Test
    void knightAtOneTwoHasExactlyEightMoves() {
        // Check if the knight has exactly 8 moves
        // Knight at (1, 2) and should be able to move to (0, 0), (0, 4), (3, 3), (0, 2), (1, 3), (4, 2)
        AbstractPiece knight = new Knight(new BoardSquare(1,2));
        assertEquals(6, knight.getEmptyBoardMoves().size());
    }

    @Test
    void knightAtZeroOneHasNoOutsideMoves() {
        // Check if the knight has no moves outside the board
        // Knight at (0, 1) and should not be able to move to (-1, -1) (3, -1) (0, -2)
        AbstractPiece knight = new Knight(new BoardSquare(0,1));
        List<AbstractBoardSquare> emptyBoardMoves = knight.getEmptyBoardMoves();
        int[][] emptyCoordinates = new int[emptyBoardMoves.size()][2];
        for (int i = 0; i < emptyBoardMoves.size(); i++) {
            emptyCoordinates[i] = emptyBoardMoves.get(i).getCoordinates();
        }
        assertFalse(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{-1,-1})));
        assertFalse(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{3,-1})));
        assertFalse(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{0,-2})));
    }
}