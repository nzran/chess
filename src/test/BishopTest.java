package test;

import main.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BishopTest {

    @BeforeEach
    void setUp() {
    }

    // Test has 100% line coverage

    @Test
    void bishopAtZeroTwo() {
        // Bishop should have 7 possible moves at starting square (0, 2)
        // Bishop at (0, 2) and should be able to move to (1,3), (2, 4), (3, 5), (4, 6), (5, 7), (1, 1), (2, 0)
        AbstractPiece bishop = new Bishop(new BoardSquare(0,2));
        List<AbstractBoardSquare> emptyBoardMoves = bishop.getEmptyBoardMoves();
        int[][] emptyCoordinates = new int[emptyBoardMoves.size()][2];
        for (int i = 0; i < emptyBoardMoves.size(); i++) {
            emptyCoordinates[i] = emptyBoardMoves.get(i).getCoordinates();
        }
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{1,1})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{2,0})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{1,3})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{2,4})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{3,5})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{4,6})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{5,7})));
    }

    @Test
    void bishopAtFiveFive() {
        // Bishop at a square which is not a starting square
        // Bishop at (5, 5) and should be able to move to (6, 6), (7, 7), (4, 6), (3, 7), (4, 4), (3, 3), (2, 2), (1, 1), (0, 0)
        AbstractPiece bishop = new Bishop(new BoardSquare(5,5));
        int[][] emptyCoordinates = new int[bishop.getEmptyBoardMoves().size()][2];
        for (int i = 0; i < bishop.getEmptyBoardMoves().size(); i++) {
            emptyCoordinates[i] = bishop.getEmptyBoardMoves().get(i).getCoordinates();
        }
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{6,6})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{7,7})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{4,6})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{3,7})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{4,4})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{3,3})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{2,2})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{1,1})));
        assertTrue(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{0,0})));
    }

    @Test
    void bishopAtFiveFiveShouldNotMove(){
        // Check that incorrect moves are not generated
        // Bishop at (5, 5) and should not be able to move to (6, 5), (7, 5), (4, 5)
        AbstractPiece bishop = new Bishop(new BoardSquare(5,5));
        int[][] emptyCoordinates = new int[bishop.getEmptyBoardMoves().size()][2];
        for (int i = 0; i < bishop.getEmptyBoardMoves().size(); i++) {
            emptyCoordinates[i] = bishop.getEmptyBoardMoves().get(i).getCoordinates();
        }
        assertFalse(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{6,5})));
        assertFalse(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{7,5})));
        assertFalse(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{4,5})));
    }

    @Test
    void bishopAtSevenTwoMoveCount() {
        // Bishop at (7, 2) and should have 7 possible moves
        AbstractPiece bishop = new Bishop(new BoardSquare(7,2));
        assertEquals(7, bishop.getEmptyBoardMoves().size());
    }

    @Test
    void bishopAtZeroZeroHasNoOutsideMoves(){
        // Check that bishop has no moves outside the board
        // Bishop at (0, 0) and should not be able to move to (-1, -1), (-2, -2), (-1, 1)
        AbstractPiece bishop = new Bishop(new BoardSquare(0,0));
        int[][] emptyCoordinates = new int[bishop.getEmptyBoardMoves().size()][2];
        for (int i = 0; i < bishop.getEmptyBoardMoves().size(); i++) {
            emptyCoordinates[i] = bishop.getEmptyBoardMoves().get(i).getCoordinates();
        }
        assertFalse(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{-1,-1})));
        assertFalse(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{-2,-2})));
        assertFalse(Arrays.stream(emptyCoordinates).anyMatch(m->Arrays.equals(m,new int[]{-1,1})));
    }
}