package main;

import java.util.List;

public class King extends AbstractPiece {
    /*
     * Added constructor to set team
     * Switched from BoardSquare to AbstractBoardSquare
     */

    public King(AbstractBoardSquare square) {
        super(square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        return null;
    }
}
