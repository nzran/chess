package main;

import java.util.List;

public class Pawn extends AbstractPiece {

    public Pawn(AbstractBoardSquare square) {
        super(square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        return null;
    }
}
