package main;

import java.util.List;

public abstract class AbstractPiece {
    /*
     * Added getters and setters for startSquare and currentSquare
     * Added constructor to set team, this also lets us make Team final
     * and avoid changing teams mid-game as that should be impossible for
     * chess piece.
     *
     * Switched from BoardSquare to AbstractBoardSquare
     */

    private final AbstractBoardSquare startSquare;
    private AbstractBoardSquare currentSquare;
    Team team; // this is unused but can be useful to identify if a piece is black or white

    public AbstractPiece(AbstractBoardSquare startSquare) {
        this.startSquare = startSquare;
        this.currentSquare = startSquare;
    }

    public abstract List<AbstractBoardSquare> getEmptyBoardMoves();

    public AbstractBoardSquare getStartSquare() {
        return startSquare;
    }

    public void setCurrentSquare(AbstractBoardSquare square) {
        this.currentSquare = square;
    }

    public AbstractBoardSquare getCurrentSquare() {
        return this.currentSquare;
    }

   }
