package main;

import java.util.ArrayList;
import java.util.List;

public class Bishop extends AbstractPiece {
    public Bishop(AbstractBoardSquare square) {
        super(square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        AbstractBoardSquare currentSquare = this.getCurrentSquare();
        List<AbstractBoardSquare> emptyBoardMoves = new ArrayList<>();

        int Y = currentSquare.getCoordinates()[1];
        int X = currentSquare.getCoordinates()[0];

        // Bishop can move diagonally in any direction
        // So, we can use a nested for loop to generate all possible moves
        // and then filter out the invalid ones
        for (int row = Y - 7; row <= Y + 7; row++) {
            if (row < 0 || row > 7) {
                continue;
            }
            for (int col = X - 7; col <= X + 7; col++) {
                if (col < 0 || col > 7) {
                    continue;
                }
                // Skip the same square
                if (row == Y && col == X) {
                    continue;
                }
                // If the move is valid, add it to the list of valid moves
                // A move is valid if the difference between the row and column is the same
                if (Math.abs(row - Y) == Math.abs(col - X)) {
                    emptyBoardMoves.add(new BoardSquare(col, row));
                }
            }
        }

        // Print moves
//        for (AbstractBoardSquare square : emptyBoardMoves) {
//            System.out.println(Arrays.toString(square.getCoordinates()));
//        }
        return emptyBoardMoves;
    }
}
