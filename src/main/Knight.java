package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Knight extends AbstractPiece{

    public Knight(AbstractBoardSquare square) {
        super(square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        AbstractBoardSquare currentSquare = this.getCurrentSquare();
        List<AbstractBoardSquare> emptyBoardMoves = new ArrayList<>();

        int currentRow = currentSquare.getCoordinates()[1];
        int currentCol = currentSquare.getCoordinates()[0];

        // Knight can take either two steps or one step in any direction
        // So, we can use a nested for loop to generate all possible moves
        // and then filter out the invalid ones
        for (int row = currentRow - 2; row <= currentRow + 2; row++) {
            if (row < 0 || row > 7) {
                continue;
            }
            for (int col = currentCol - 2; col <= currentCol + 2; col++) {
                if (col < 0 || col > 7) {
                    continue;
                }
                // If the move is invalid, skip it
                if (row == currentCol && col == currentRow) {
                    continue;
                }
                // If the move is valid, add it to the list of valid moves
                // A move is valid if difference between location is 3
                if (Math.abs(row - currentRow) + Math.abs(col - currentCol) == 3) {
                    emptyBoardMoves.add(new BoardSquare(col, row));
                }
            }
        }
        // Print knight moves
//        for (AbstractBoardSquare square : emptyBoardMoves) {
//            System.out.println(Arrays.toString(square.getCoordinates()));
//        }
        return emptyBoardMoves;
    }
}
