package main;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class Board {
    /*
     * Board has bottom left square as (0,0)
     * and top right square as (7,7)
     */

    /* 1.
     * Board and Player have bidirectional relationship
     * Since Board will have a player to move a piece
     * as well as Player will have a board on which to move it
     */

    /* 2.
     * Changed BoardSquare to AbstractBoardSquare and resolved
     * Dependency Inversion
     */

    /* 3.
     * Since Board and AbstractPiece both need to know the same
     * BoardSquare, we can remove this dependency by moving
     * the BoardSquare held by the piece to AbstractPiece, reducing coupling.
     */

    /* 4.
     * Since Each player has their own pieces to play
     * we can remove this dependency by moving the pieces
     * held by the player to Player, reducing coupling.
     */
    //private List<AbstractPiece> pieces = new ArrayList<>();

    // Changed type from BitSet to List<AbstractBoardSquare>
    private final List<AbstractBoardSquare> occupiedSquares;

    // King is not used but can have a use in the future
    private final List<King> kings;

    /*
     * Board will be the Creator of BoardSquares.
    */
    private final List<List<AbstractBoardSquare>> boardSquares;

    public Board() {
        occupiedSquares = new ArrayList<>();
        boardSquares = new ArrayList<>();
        kings = new ArrayList<>();
        // Create 8x8 BoardSquares as 2D List
        for (int i = 0; i < 8; i++) {
            List<AbstractBoardSquare> row = new ArrayList<>();
            for (int j = 0; j < 8; j++) {
                // Create BoardSquare
                row.add(new BoardSquare(j, i));
            }
            boardSquares.add(row);
        }
    }

    public List<List<AbstractBoardSquare>> boardSquares() {
        return boardSquares;
    }

    public void occupySquare(AbstractBoardSquare s) {
        occupiedSquares.add(s);
    }

    public boolean movePiece(AbstractPiece p, AbstractBoardSquare b) {
        // Check if the square is empty
        if (isEmpty(b)) {
            // Move the piece to the new square
            p.setCurrentSquare(b);
            return true;
        }
        return false;
    }

    // We only need one method for this as every piece is AbstractPiece
    public List<AbstractBoardSquare> getValidMoves(AbstractPiece p) {
        // Since we have AbstractPiece we will not need
        // to check instanceof a particular piece anymore
        // like it shows in sequence diagram
        List<AbstractBoardSquare> validMoves = p.getEmptyBoardMoves();
        for (AbstractBoardSquare s : validMoves) {
            boolean canMove = checkClearPath(p, s);
            if (!canMove) {
                validMoves.remove(s);
            }
        }
        return validMoves;
    }

    private boolean checkClearPath(AbstractPiece p, AbstractBoardSquare s) {
        // Check if path is clear
        // This method is not implemented for an actual board
        // but for our test since we test a single piece at a time
        // we can simply return true
        return true;
    }

    public boolean isEmpty(AbstractBoardSquare s) {
        return !occupiedSquares.contains(s);
    }


    /*
     * I did this preemptively to make a board but was not needed.
     *
     * Board will have two players
     */
//    private Player player1;
//    private Player player2;
//
//    public void generateBoard() {
//        // Create Players
//        player1 = new Player("white", this);
//        player2 = new Player("black", this);
//
//        // Create 32 Pieces
//        // Create Pawns
//        for (int i = 0; i < 8; i++) {
//            player1.addPiece(new Pawn(boardSquares.get(1).get(i)));
//            player2.addPiece(new Pawn(boardSquares.get(6).get(i)));
//        }
//        // Create Rooks
//        player1.addPiece(new Rook(boardSquares.get(0).get(0)));
//        player1.addPiece(new Rook(boardSquares.get(0).get(7)));
//        player2.addPiece(new Rook(boardSquares.get(7).get(0)));
//        player2.addPiece(new Rook(boardSquares.get(7).get(7)));
//        // Create Knights
//        player1.addPiece(new Knight(boardSquares.get(0).get(1)));
//        player1.addPiece(new Knight(boardSquares.get(0).get(6)));
//        player2.addPiece(new Knight(boardSquares.get(7).get(1)));
//        player2.addPiece(new Knight(boardSquares.get(7).get(6)));
//        // Create Bishops
//        player1.addPiece(new Bishop(boardSquares.get(0).get(2)));
//        player1.addPiece(new Bishop(boardSquares.get(0).get(5)));
//        player2.addPiece(new Bishop(boardSquares.get(7).get(2)));
//        player2.addPiece(new Bishop(boardSquares.get(7).get(5)));
//        // Create Queens
//        player1.addPiece(new Queen(boardSquares.get(0).get(3)));
//        player2.addPiece(new Queen(boardSquares.get(7).get(3)));
//        // Create Kings
//        King whiteKing = new King(boardSquares.get(0).get(4));
//        King blackKing = new King(boardSquares.get(7).get(4));
//        kings.add(whiteKing);
//        kings.add(blackKing);
//        player1.addPiece(whiteKing);
//        player2.addPiece(blackKing);
//    }

}
