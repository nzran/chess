package main;

import java.util.List;

public class Rook extends AbstractPiece {
    public Rook(AbstractBoardSquare square) {
        super(square);
    }

    @Override
    public List<AbstractBoardSquare> getEmptyBoardMoves() {
        return null;
    }
}
