package main;

public class Team {
    private final String color;

    public Team(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
