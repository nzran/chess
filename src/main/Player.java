package main;

import java.util.*;

public class Player {
    private final String name;
    private final List<AbstractPiece> pieces;
    private final Board board;

    public Player(String name, Board board) {
        this.name = name;
        this.board = board;
        this.pieces = new ArrayList<>();
    }

    public List<AbstractPiece> getPieces() {
        return pieces;
    }

    public void addPiece(AbstractPiece piece){
        pieces.add(piece);
        board.occupySquare(piece.getCurrentSquare());
    }

    public void takeNextTurn(){
        Map<AbstractPiece, List<AbstractBoardSquare>> validMoves = new HashMap<>();
        for (AbstractPiece piece : pieces) {
            List<AbstractBoardSquare> s = board.getValidMoves(piece);
            validMoves.put(piece, s);
        }
        if (board.movePiece(pieces.get(0), validMoves.get(pieces.get(0)).get(0))) {
            // Move was successful, some future function
            // like animating the move on the board

        }
    }

    public String getName() {
        return name;
    }
}
